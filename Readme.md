sls-flask-restful-template
==========================

Features
--------

- Flask-restful
- mypy
- 100% test coverage
- moto for aws mocks during test
- pynamodb
- serverless
- serverless-wsgi
- serverless-python-requirements
- AWS X-Ray
- custom authorizer