from pyauthlib import UserInfo, AuthPolicy, HttpMethod, parse_event, raise_401


def lambda_handler(event, _context):
    event = parse_event(event)

    if event.access_token != 'token':
        raise_401()

    user_info = UserInfo('user1', [])
    policy = AuthPolicy(user_info)
    policy.allow(event.arn(method=HttpMethod.ALL, resource='*'))

    return policy.build()
