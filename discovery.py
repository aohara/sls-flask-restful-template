import boto3
from requests import Session


class _ServiceSession(Session):

    def __init__(self, prefix_url=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.prefix_url = prefix_url

    def request(self, method, url, *args, **kwargs):
        url = self.prefix_url + url
        response = super().request(method, url, *args, **kwargs)
        return response


class Discovery(object):

    def __init__(self, service: str, stage: str = 'prod'):
        stack_name = f'{service}-{stage}'
        self.stack = boto3.resource('cloudformation').Stack(stack_name)
        try:
            assert self.stack.stack_id
        except Exception:
            raise ValueError('stack not found: ' + stack_name)

    def _output(self, name: str) -> str:
        for output in self.stack.outputs:
            if output['OutputKey'] == name:
                return output['OutputValue']
        raise ValueError(name)

    def api(self) -> Session:
        endpoint = self._output('ServiceEndpoint')
        return _ServiceSession(endpoint)

    def table_name(self, logical_id: str) -> str:
        table = self.stack.Resource(logical_id)
        return table.physical_resource_id
