from things.things_app import create_app
from things.thing import Thing

from moto import mock_dynamodb2

if __name__ == '__main__':
    with mock_dynamodb2():
        app = create_app('things')
        Thing.create_table(read_capacity_units=1, write_capacity_units=1)
        app.run(port=5000, debug=True)
