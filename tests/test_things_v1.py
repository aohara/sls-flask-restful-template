from os import environ

import pytest
from moto import mock_dynamodb2
from flask import Flask

from things.things_v1 import create_blueprint
from things.thing import Thing


@pytest.fixture(name="client")
def client_fixture():
    app = Flask(__name__)
    app.register_blueprint(create_blueprint("things"))
    app.config["TESTING"] = True
    app.config["DEBUG"] = True

    environ['AWS_ACCESS_KEY_ID'] = 'foobar_id'
    environ['AWS_SECRET_ACCESS_KEY'] = 'foobar_secret'
    with mock_dynamodb2():
        Thing.create_table(read_capacity_units=1, write_capacity_units=1)
        yield app.test_client()


@pytest.fixture(name="thing1")
def thing1_fixture() -> Thing:
    comment = Thing(name="Thing 1")
    comment.save()
    return comment


def test_list_missing(client):
    r = client.get("/")
    assert r.get_json() == list()


def test_list_populated(client, thing1: Thing):
    response = client.get("/")
    assert response.status_code == 200

    items = response.get_json()
    assert 1 == len(items)
    item1 = items[0]
    assert item1["uuid"] == thing1.uuid
    assert item1["name"] == thing1.name
    assert item1["created"]


def test_create(client):
    response = client.post("/", data=dict(name="thing2"))
    assert response.status_code == 200

    content = response.get_json()
    assert content["uuid"]
    assert content["name"] == "thing2"
    assert content["created"]


def test_create_invalid(client):
    response = client.post("/", data=dict())
    assert response.status_code == 400


def test_delete_missing(client):
    response = client.delete("/id1")
    assert response.status_code == 404


def test_delete(client, thing1: Thing):
    response = client.delete(f"/{thing1.uuid}")
    assert response.status_code == 200
    assert response.get_json()["uuid"] == thing1.uuid


def test_update_not_found(client):
    response = client.put("/id1", data=dict(name="thing2"))
    assert response.status_code == 404


def test_update(client, thing1: Thing):
    response = client.put(f"/{thing1.uuid}", data=dict(name="Thing One"))
    assert response.status_code == 200

    updated = response.get_json()
    assert updated["uuid"] == thing1.uuid
    assert updated["name"] == "Thing One"


def test_update_invalid(client, thing1: Thing):
    response = client.put(f"/{thing1.uuid}", data=dict())
    assert response.status_code == 400
