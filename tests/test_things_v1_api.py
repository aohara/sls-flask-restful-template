from os import environ

import pytest

from tests.things_driver import ThingsDriver as Driver
from discovery import Discovery


@pytest.mark.api
class TestApi(object):

    @pytest.fixture(name="driver")
    def driver_fixture(self) -> Driver:
        stage: str = environ.get('STAGE') or 'dev'
        discovery = Discovery('sample', stage)
        return Driver(discovery.api(), headers=dict(Authorization='Bearer token'))

    @pytest.fixture(name="thing1")
    def thing1_fixture(self, driver: Driver):
        thing = driver.create('Thing 1')
        yield thing

        uuid = thing['uuid']
        driver.delete(uuid)

    def test_list(self, driver: Driver, thing1: dict):
        items = list(driver)

        for item in items:
            if item['uuid'] == thing1['uuid']:
                assert item["name"] == thing1['name']
                assert item["created"]
                return
        assert False

    def test_create(self, driver: Driver):
        content = driver.create('thing2')
        uuid = content['uuid']

        assert uuid
        assert content["name"] == "thing2"
        assert content["created"]

        driver.delete(uuid)

    def test_delete(self, driver: Driver, thing1: dict):
        uuid = thing1['uuid']

        deleted = driver.delete(uuid)
        assert deleted['uuid'] == uuid

    def test_delete_missing(self, driver: Driver):
        assert driver.delete('foo') is None

    def test_update(self, driver: Driver, thing1: dict):
        updated = driver.update(thing1['uuid'], 'Thing One')

        assert updated["uuid"] == thing1['uuid']
        assert updated["name"] == "Thing One"

    def test_update_missing(self, driver: Driver):
        thing = driver.update('123', 'thing1')
        assert thing is None


@pytest.mark.api
class TestApiSecurity(object):

    @pytest.fixture(name="driver")
    def driver_fixture(self) -> Driver:
        stage: str = environ.get('STAGE') or 'dev'
        discovery = Discovery('sample', stage)
        return Driver(discovery.api())

    def test_unauthenticated(self, driver):
        r = driver.session.get('/things/1/')
        assert r.status_code == 401

    def test_invalid_token(self, driver):
        r = driver.session.get('/things/1/', headers=dict(Authorization='Bearer foo'))
        assert r.status_code == 401

    def test_list(self, driver):
        r = driver.session.get('/things/1/', headers=dict(Authorization='Bearer token'))
        assert r.status_code == 200
