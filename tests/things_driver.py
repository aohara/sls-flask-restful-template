from typing import Optional

from requests import Session


class ThingsDriver(object):

    def __init__(self, session: Session, headers: dict = None):
        self.session = session
        self.session.headers.update(headers or dict())

    def __getitem__(self, uuid: str) -> Optional[dict]:
        r = self.session.get(f'/things/1/{uuid}')
        if r.ok:
            thing = r.json()
            assert thing['uuid'] == uuid
            return thing
        elif r.status_code == 404:
            return None
        raise ValueError(r)

    def __iter__(self):
        r = self.session.get('/things/1/')

        assert r.ok, r
        yield from r.json()

    def create(self, name: str) -> dict:
        r = self.session.post(
            '/things/1/',
            data=dict(name=name)
        )

        assert r.status_code == 200, r.status_code
        thing = r.json()
        assert thing['name'] == name
        return thing

    def update(self, uuid: str, name: str) -> Optional[dict]:
        r = self.session.put(
            f'/things/1/{uuid}',
            data=dict(name=name)
        )

        if r.ok:
            thing = r.json()
            assert thing['uuid'] == uuid
            assert thing['name'] == name
            return thing
        elif r.status_code == 404:
            return None
        raise ValueError(r)

    def __delitem__(self, uuid: str):
        return self.delete(uuid)

    def delete(self, uuid: str) -> Optional[dict]:
        r = self.session.delete(f'/things/1/{uuid}')

        if r.ok:
            thing = r.json()
            assert thing['uuid'] == uuid
            return thing
        elif r.status_code == 404:
            return None
        raise ValueError(r)
