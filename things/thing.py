from uuid import uuid4
from datetime import datetime
from typing import Optional

from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute, UTCDateTimeAttribute


class Thing(Model):
    class Meta:
        table_name: Optional[str] = None

    uuid = UnicodeAttribute(hash_key=True, default=lambda: str(uuid4()))
    created = UTCDateTimeAttribute(null=False, default=lambda: datetime.utcnow())
    name = UnicodeAttribute(null=False)
