from flask import Flask
from things import things_v1


def create_app(things_table_name: str) -> Flask:
    app = Flask(__name__)
    v1_bp = things_v1.create_blueprint(things_table_name)
    app.register_blueprint(v1_bp, url_prefix="/things/1")
    return app
