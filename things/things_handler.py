from os import environ

import awsgi
from aws_xray_sdk.core import xray_recorder, patch_all
from aws_xray_sdk.core.context import Context
from aws_xray_sdk.ext.flask.middleware import XRayMiddleware

from things.things_app import create_app

APP = create_app(environ["THINGS_TABLE_NAME"])

# Setup AWS X-ray instrumentation
xray_recorder.current_segment()
xray_recorder.configure(
    service="sls-flask-restful-template", sampling=False, context=Context()
)
XRayMiddleware(APP, xray_recorder)  # patch flask application
patch_all()  # patch 3rd party libraries (boto3, requests, etc.)


def lambda_handler(event, context):
    return awsgi.response(APP, event, context)
