from typing import List

from flask import abort, Blueprint
from flask_restful import Resource, fields, marshal_with, Api
from flask_restful.reqparse import RequestParser

from things.thing import Thing

_THING_FIELDS = dict(
    uuid=fields.String, name=fields.String, created=fields.DateTime("iso8601")
)


class ThingsResource(Resource):

    CREATE_ARGS: RequestParser = RequestParser()
    CREATE_ARGS.add_argument("name", type=str, required=True)

    @marshal_with(_THING_FIELDS)
    def post(self) -> Thing:
        args = self.CREATE_ARGS.parse_args()
        thing: Thing = Thing(name=args["name"])
        thing.save()
        return thing

    @marshal_with(_THING_FIELDS)
    def get(self) -> List[Thing]:
        return list(Thing.scan())


class ThingResource(Resource):

    UPDATE_ARGS: RequestParser = RequestParser()
    UPDATE_ARGS.add_argument("name", type=str, required=True)

    @marshal_with(_THING_FIELDS)
    def delete(self, uuid: str) -> Thing:
        try:
            thing: Thing = Thing.get(uuid)
            thing.delete()
            return thing
        except Thing.DoesNotExist:
            return abort(404)

    @marshal_with(_THING_FIELDS)
    def put(self, uuid: str) -> Thing:
        args = self.UPDATE_ARGS.parse_args()
        try:
            thing = Thing.get(uuid)
            thing.name = args["name"]
            thing.save()
            return thing
        except Thing.DoesNotExist:
            return abort(404)


def create_blueprint(things_table_name: str) -> Blueprint:
    Thing.Meta.table_name = things_table_name

    blueprint = Blueprint("things-v1", __name__)
    api = Api(blueprint)
    api.add_resource(ThingsResource, "/")
    api.add_resource(ThingResource, "/<string:uuid>")

    return blueprint
